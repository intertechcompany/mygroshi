import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import api from './api'
import axios from 'axios'
import {getCookie} from './utils/cookie'

import './assets/style/main.scss'

Vue.config.productionTip = false;

if(typeof getCookie('token') !== 'undefined') {
  axios.defaults.headers.common['Accept'] = 'application/json';
  axios.defaults.headers.common['Authorization'] = 'Bearer ' + getCookie('token');
} else {
  window.location.href = api.siteUrl;
}

Vue.prototype.$axios = axios;
Vue.prototype.$api = api;

Vue.filter('plural', function (value, forms) {
  return forms[(value % 10 === 1 && value % 100 !== 11 ? 0 : value % 10 >= 2 && value % 10 <= 4 && (value % 100 < 10 || value % 100 >= 20) ? 1 : 2) ];
});

Vue.filter('date', function (date) {
  let newDate = new Date(date);
  return newDate.toLocaleDateString("ru-RU");
});

new Vue({
  router,
  render: h => h(App),
}).$mount('#app');
