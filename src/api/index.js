const api = {
  baseUrl: 'https://admin.my-tenge.info',
  siteUrl: 'https://my-tenge.info',
  apiUrl: {
    offer: '/api/offer',
    offerUse: '/api/offer/use',
    me: '/api/auth/me',
    login: '/api/auth/login',
    subscription_start: '/api/payment/subscription_start',
  },
  links: {
    documents: '/documents',
    premium: '/premium',
    contacts: '/contacts',
    account: '/myaccount',
    unsubscribe: '/unsubscribe',
  }
};

export default api
