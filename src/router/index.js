import Vue from 'vue'
import VueRouter from 'vue-router'
import api from '../api'
import {getCookie} from '../utils/cookie'

Vue.use(VueRouter);

  const routes = [
  {
    path: '/',
    redirect: '/offers',
  },
  {
    path: '/offers',
    name: 'Microloan',
    component: () => import('../components/Microloan')
  },
  {
    path: '/approved/:id',
    name: 'Approved',
    component: () => import('../components/Approved')
  },
  {
    path: '/premium',
    name: 'Premium',
    component: () => import('../components/Premium')
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
  scrollBehavior (to, from, savedPosition) {
    return { x: 0, y: 0 }
  },
});

router.beforeEach((to, from, next) => {
  if (typeof getCookie('token') !== 'undefined') {
    next()
  }
  else {
    window.location.href = api.siteUrl;
  }
});

export default router;
